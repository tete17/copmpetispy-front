import React from "react";

const AppFooter = () => {
    return (
        <footer className="footer">
            <div className="container">
                <p className="text-muted">Designed by <a href="mailto:miguel.a.j82@gmail.com">@thar</a></p>
            </div>
        </footer>
    );
}

export default AppFooter;