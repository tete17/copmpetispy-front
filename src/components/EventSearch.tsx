import React, {useState, useEffect} from "react";
import {useParams, useHistory, useLocation} from "react-router-dom";
import {
    distances,
    relayDistances,
    styles,
    genders,
    categories,
    relayCategories,
    inscriptions_api_uri,
} from "../values"
import {eventRelaySearchColumns, eventSearchTableColumn} from "../tableColumns";
import CompetyTableController from "./CompetyTableController";


const EventSearchT = ({searchFilter}: any) => {
    const isRelay = searchFilter.includes("event_type=relay");
    return (<>
        {
            isRelay
          ? <CompetyTableController columnsDefinition={eventRelaySearchColumns} searchFilter={searchFilter} />
          : <CompetyTableController columnsDefinition={eventSearchTableColumn} searchFilter={searchFilter} />
        }
    </>);
};

const EventSearch = () => {
    const { championshipId, distanceId, styleId, genderId, categoryId, isRelayId } = useParams();
    const championship = championshipId ? championshipId : "";
    const urlIncludesData = championshipId && distanceId && styleId && genderId && categoryId && isRelayId;
    const [formData, setFormData] = useState({
        distance: distanceId ? parseInt(distanceId) : 50,
        style: styleId ? styleId.toUpperCase() : 'MARIPOSA',
        gender: genderId ? genderId.toUpperCase() : "MASCULINO",
        category: categoryId ? categoryId : "25",
        isRelay: isRelayId ? isRelayId == "1" : false
    });
    const getSearchFilterFromFromData = () => {
        if (!urlIncludesData) { return ""; }
        const event_type = formData.isRelay ? "relay" : "individual";
        const categoryPrefix = formData.isRelay ? "%2B" : "";
        return `${inscriptions_api_uri}?championship_name=${encodeURI(championship)}&event_distance=${formData.distance}&event_style=${formData.style}&event_gender=${formData.gender}&event_category=${categoryPrefix + formData.category}&event_type=${event_type}&results_limit=100`;
    };
    const [availableDistances, setAvailableDistances] = useState(formData.isRelay ? relayDistances : distances);
    const [availableCategories, setAvailableCategories] = useState(formData.isRelay ? relayCategories : categories);
    const [searchFilter, setSearchFilter] = useState(getSearchFilterFromFromData());
    const handleIsRelayChange = (event: any) => {
        const newIsRelay = event.target.checked;
        let change: any = {...formData};
        change.isRelay = newIsRelay;
        setFormData(change);

        if (newIsRelay) {
            setAvailableDistances(relayDistances);
            setAvailableCategories(relayCategories);
        } else {
            setAvailableDistances(distances);
            setAvailableCategories(categories);
        }
    };
    const handleFormChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = event.target.value;
        setFormData(change);
    };
    const history = useHistory();
    const handleSubmit = (event: any) => {
        event.preventDefault();
        const newPath = "/"+["eventsearch", championship, formData.distance, formData.style, formData.gender, formData.category, formData.isRelay?"1":"0"].join("/");
        history.push(newPath);
        setSearchFilter(getSearchFilterFromFromData());
    };
    const location = useLocation();
    useEffect(() => {
        setSearchFilter(getSearchFilterFromFromData());
    }, [location]);
    return (
        <div>
            <div>
                <div className="form-row align-items-center">
                    <div className="form-check form-row">
                        <input type="checkbox" name="isRelay" id="isRelay" className="form-check-input" checked={formData.isRelay} onChange={handleIsRelayChange} />
                        <label htmlFor="isRelay">Buscar relevo</label>
                    </div>
                    <div className="form-group col-auto">
                        <label htmlFor="distancia">Distancia:</label>
                        <select id="distancia" className="form-control" name="distance" value={formData.distance} onChange={handleFormChange}>
                            {availableDistances.map((distance) => <option key={distance} value={distance}>{formData.isRelay ? "4x" : ""}{distance}m</option>)}
                        </select>
                    </div>
                    <div className="form-group col-auto">
                        <label htmlFor="estilo">Estilo:</label>
                        <select id="estilo" className="form-control" name="style" value={formData.style} onChange={handleFormChange}>
                            {styles.map((style) => <option key={style} value={style.toUpperCase()}>{style}</option>)}
                        </select>
                    </div>
                    <div className="form-group col-auto">
                        <label htmlFor="genero">Genero:</label>
                        <select id="genero" className="form-control" name="gender" value={formData.gender} onChange={handleFormChange}>
                            {genders.map((gender) => <option key={gender} value={gender.toUpperCase()}>{gender}</option>)}
                        </select>
                    </div>
                    <div className="form-group col-auto">
                        <label htmlFor="categoria">Categoria:</label>
                        <select id="categoria" className="form-control" name="category" value={formData.category} onChange={handleFormChange}>
                            {availableCategories.map((cat) => <option key={cat} value={cat}>{cat}</option>)}
                        </select>
                    </div>
                    <div className="col-auto">
                        <button id="buscar" className="btn btn-primary" onClick={handleSubmit}>Buscar</button>
                    </div>
                </div>
            </div>
            {searchFilter === ""
                ? <></>
                : <EventSearchT searchFilter={searchFilter} />
            }
        </div>
    );
};

export default EventSearch;