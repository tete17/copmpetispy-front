import React, {useCallback, useEffect, useState} from "react";
import CompetyTable from "./CompetyTable";
import SpyTable from "./SpyTable";

const CompetyTableController = ({columnsDefinition, searchFilter}: any) => {
    const [data, setSwimmers] = useState([]);

    useEffect(() => {
        (async () => {
            fetch(searchFilter)
                .then((res) => {
                    if (res.ok) {
                        return res.json()
                    } else {
                        throw new Error(res.statusText);
                    }
                })
                .then(res => setSwimmers(res))
                .catch(err => console.log('Error: ', err))
        })();
    }, [searchFilter]);
    const renderSpyComponent = useCallback(
        ({ row }: any) => (
            <SpyTable {...row.values}/>
        ),
        []
    )
    return(
        <CompetyTable data={data} columns={columnsDefinition} renderRowSubComponent={renderSpyComponent} />
    );
}

export default CompetyTableController;