import React, {useState, useEffect} from "react";
import {useParams, useHistory, useLocation} from "react-router-dom";
import {
    range,
    distances,
    styles,
    genders,
    inscriptions_api_uri
} from "../values"
import {swimmerSearchTableColumns} from "../tableColumns";
import CompetyTableController from "./CompetyTableController";

const AdvancedSearch = () => {
    const { championshipId, clubId, distanceId, styleId, genderId, bornFromId, bornUntilId } = useParams();
    const urlIncludesData = championshipId && clubId && distanceId && styleId && genderId && bornFromId && bornUntilId;
    const championship = championshipId ? championshipId : "";
    const actualYear = new Date(Date.now()).getFullYear();
    const [formData, setFormData] = useState({
        club: clubId ? clubId : '',
        event_distance: distanceId ? parseInt(distanceId) : 0,
        event_style: styleId ? styleId.toUpperCase() : "0",
        event_gender: genderId ? genderId.toUpperCase() : "0",
        participant_born_year_gte: bornFromId ? parseInt(bornFromId) : 0,
        participant_born_year_lt: bornUntilId ? parseInt(bornUntilId) : 0,
    });
    const getSearchFilterFromFromData = () => {
        if (!urlIncludesData) { return ""; }

        if (formData.club.length < 3) { return; }
        let newSearchFilter = `${inscriptions_api_uri}?championship_name=${encodeURI(championship)}&club=${encodeURI(formData.club)}&results_limit=100&event_type=individual`
        if (formData.event_distance !== 0) {
            newSearchFilter += `&event_distance=${formData.event_distance}`
        }
        if (formData.event_style !== "0") {
            newSearchFilter += `&event_style=${formData.event_style}`
        }
        if (formData.event_gender !== "0") {
            newSearchFilter += `&event_gender=${formData.event_gender}`
        }
        if (formData.participant_born_year_gte !== 0) {
            newSearchFilter += `&participant_born_year_gte=${formData.participant_born_year_gte}`
        }
        if (formData.participant_born_year_lt !== 0) {
            newSearchFilter += `&participant_born_year_lt=${formData.participant_born_year_lt}`
        }
        return newSearchFilter;
    };
    const [searchFilter, setSearchFilter] = useState(getSearchFilterFromFromData());
    const handleFormChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = event.target.value;
        setFormData(change);
    };
    const handleNumberFormChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = parseInt(event.target.value, 10);
        setFormData(change);
    };
    const history = useHistory();
    const handleSubmit = (event: any) => {
        event.preventDefault();
        const newPath = "/" + ["advancedsearch", championship, formData.club, formData.event_distance, formData.event_style, formData.event_gender, formData.participant_born_year_gte, formData.participant_born_year_lt].join("/");
        history.push(newPath);
        setSearchFilter(getSearchFilterFromFromData());
    };
    const location = useLocation();
    useEffect(() => {
        setSearchFilter(getSearchFilterFromFromData());
    }, [location]);
    const bottomStyle ={
        height: "auto",
        marginTop: "auto",
        marginBottom: "15px"
    };
    return (
        <>
            <div className="form-row align-items-center">
                <div className="col-auto form-group" style={bottomStyle}>
                    <input type="text" name="club" placeholder="Club" className="form-control" value={formData.club} onChange={handleFormChange} />
                </div>
                <div className="col-auto form-group">
                    <label htmlFor="event_distance">Distancia:</label>
                    <select id="event_distance" name="event_distance" className="form-control" value={formData.event_distance} onChange={handleNumberFormChange} >
                        <option value="0">Todas</option>
                        {distances.map((distance) => <option key={distance} value={distance}>{distance}m</option>)}
                    </select>
                </div>
                <div className="col-auto form-group">
                    <label htmlFor="event_style">Estilo:</label>
                    <select id="event_style" name="event_style" className="form-control" value={formData.event_style} onChange={handleFormChange} >
                        <option value="0">Todos</option>
                        {styles.map((style) => <option key={style} value={style.toUpperCase()}>{style}</option>)}
                    </select>
                </div>
                <div className="col-auto form-group">
                    <label htmlFor="event_gender">Genero:</label>
                    <select id="event_gender" name="event_gender" className="form-control" value={formData.event_gender} onChange={handleFormChange} >
                        <option value="0">Todos</option>
                        {genders.map((gender) => <option key={gender} value={gender.toUpperCase()}>{gender}</option>)}
                    </select>
                </div>
                <div className="col-auto form-group">
                    <label htmlFor="participant_born_year_gte">Nacidos desde:</label>
                    <select id="participant_born_year_gte" name="participant_born_year_gte" className="form-control" value={formData.participant_born_year_gte} onChange={handleNumberFormChange} >
                        <option value="0">Todos</option>
                        {range(actualYear - 105, actualYear - 19, 1).map((year) => <option key={"gte"+year} value={year}>{year}</option>)}
                    </select>
                </div>
                <div className="col-auto form-group">
                    <label htmlFor="participant_born_year_lt">Nacidos hasta (no se incluye):</label>
                    <select id="participant_born_year_lt" name="participant_born_year_lt" className="form-control" value={formData.participant_born_year_lt} onChange={handleNumberFormChange} >
                        <option value="0">Todos</option>
                        {range(actualYear - 105, actualYear - 19, 1).map((year) => <option key={"lt"+year} value={year}>{year}</option>)}
                    </select>
                </div>
                <div className="col-auto" style={bottomStyle}>
                    <button className="align-self-end btn btn-lg btn-block btn-primary" onClick={handleSubmit}>Buscar</button>
                </div>
            </div>
            {searchFilter === "" ? <></> : <CompetyTableController columnsDefinition={swimmerSearchTableColumns} searchFilter={searchFilter}/> }
        </>
    );
};

export default AdvancedSearch;