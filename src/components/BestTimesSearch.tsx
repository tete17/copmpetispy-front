import React, {useState} from "react";
import {useParams, useHistory} from "react-router-dom";
import SpyTable from "./SpyTable";

const BestTimesSearch = () => {
    const { nameId, surnameId, bornYearId } = useParams();
    const [participantData, setParticipantData] = useState({
        participant: {
            leverade: {
                name: nameId ? nameId : '',
                surname: surnameId ? surnameId : ''
            },
            born_year: bornYearId ? parseInt(bornYearId) : 0
        }
    });
    const [formData, setFormData] = useState({
        participant_name: nameId ? nameId : '',
        participant_surname: surnameId ? surnameId : '',
        participant_born_year: bornYearId ? bornYearId : '',
    });
    const handleFormChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = (event.target.validity.valid) ? event.target.value : change[event.target.name];
        setFormData(change);
    };
    const history = useHistory();
    const handleSubmit = (event: any) => {
        let change: any = {...participantData};
        change.participant.leverade.name = formData.participant_name;
        change.participant.leverade.surname = formData.participant_surname;
        change.participant.born_year = parseInt(formData.participant_born_year, 10);
        event.preventDefault();
        const newPath = "/" + ["swimmertimes", formData.participant_name, formData.participant_surname, formData.participant_born_year].join("/");
        history.push(newPath);
        setParticipantData(change);
    };
    const searchButtonStyle ={
        height: "auto",
        marginTop: "auto",
        marginBottom: "10px"
    };
    return(
        <>
        <div className="form-row align-items-center">
            <div className="col-auto">
                <div className="form-group form-row">
                    <input type="text" name="participant_name" placeholder="nombre"
                           value={formData.participant_name} onChange={handleFormChange} className="form-control"/>
                </div>
            </div>
            <div className="col-auto">
                <div className="form-group form-row">
                    <input type="text" name="participant_surname" placeholder="apellidos"
                           value={formData.participant_surname} onChange={handleFormChange} className="form-control"/>
                </div>
            </div>
            <div className="col-auto">
                <div className="form-group form-row">
                    <input type="tel" name="participant_born_year" placeholder="año nacimiento" pattern="\d?\d?\d?\d?"
                           value={formData.participant_born_year} onChange={handleFormChange} className="form-control"/>
                </div>
            </div>
            <div className="col-auto" style={searchButtonStyle}>
                <button className="align-self-end btn btn-lg btn-block btn-primary" onClick={handleSubmit}>Buscar</button>
            </div>
        </div>
        {participantData.participant.leverade.name === "" ? <></> : <SpyTable {...participantData} />}
        </>
    );
};

export default BestTimesSearch;