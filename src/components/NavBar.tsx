import React, {useEffect, useState} from "react";
import {NavLink, useHistory} from "react-router-dom";
import {championships_api_uri} from "../values";

const NavBar = ({setChampionship}: any) => {
    const [championships, setChampionships] = useState([]);
    const [navChampionship, setNavChampionship] = useState("");

    let history = useHistory();

    const handleChampionshipChange = (event: any) => {
        const newChampionship = event.target.textContent;
        setChampionship(event.target.textContent);
        setNavChampionship(event.target.textContent);
        const oldLocation = window.location.pathname;
        if (oldLocation.startsWith("/swimmertimes")) { return; }
        const availableParts = ["eventsearch", "swimmersearch", "advancedsearch", "test"];
        const locationParts = oldLocation.split('/').slice(1);
        if (!availableParts.includes(locationParts[0])) { return; }
        const newLocation = "/"+locationParts[0] + "/" + encodeURIComponent(newChampionship) + "/" + locationParts.slice(2).join("/");
        history.push(newLocation);
    };

    const getUrlWithChampionship = (page: string) => {
        return "/" + page + "/"+encodeURI(navChampionship)
    };

    useEffect(() => {
        (async () => {
            fetch(championships_api_uri)
                .then(res => res.json())
                .then(
                    (result) => {
                        setChampionships(result);
                        for (let champ of result) {
                            if (window.location.pathname.includes(encodeURIComponent(champ))) {
                                setChampionship(champ);
                                setNavChampionship(champ);
                                return;
                            }
                        }
                        setChampionship(result[0]);
                        setNavChampionship(result[0]);
                    },
                    (error) => {
                        console.log(error);
                    }
                )
        })();
    }, []);
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/#">CompetiSpy</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" exact to={getUrlWithChampionship("eventsearch")}>Event<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" to={getUrlWithChampionship("swimmersearch")}>Swimmer<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" to={getUrlWithChampionship("advancedsearch")}>Advanced search<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Championship
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                            {championships.map((championship) => <span key={championship} className="dropdown-item" onClick={handleChampionshipChange}>{championship}</span>)}
                        </div>
                    </li>
                    <li className="nav-item"><hr className="border-top" /></li>
                    <li className="nav-item">
                        <NavLink className="nav-link" activeClassName="active" to="/swimmertimes">Swimmer best times search<span className="sr-only">(current)</span></NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default NavBar;