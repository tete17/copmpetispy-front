import React from "react";

const SpyRenderer = ({ row, values }: any) => {
    return 'leverade' in values ? (<><span {...row.getToggleRowExpandedProps()}>
            {row.isExpanded ? '👇' : '👉'}
          </span><i data-first_name={values.leverade.name}
                                      data-last_name={values.leverade.surname}
                                      data-birth_year={values.born_year} className="fa fa-search time-search" /></>) : (<></>);
};

export default SpyRenderer;