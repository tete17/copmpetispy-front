import React from "react";

const EventRenderer = ({ values }: any) => {
    const prefix = values.type === "relay" ? "4x" : "";
    return <>{prefix + values.distance.toString() + " " + values.style + " " + values.gender}</>

};

export default EventRenderer;