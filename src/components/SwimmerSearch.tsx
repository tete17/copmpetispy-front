import React, {useState, useEffect} from "react";
import {useParams, useHistory, useLocation} from "react-router-dom";
import CompetyTableController from "./CompetyTableController";
import {swimmerSearchTableColumns} from "../tableColumns";
import {inscriptions_api_uri} from "../values";

const SwimmerSearch = () => {
    const { championshipId, swimmerNameId, isLicenseId } = useParams();
    const championship = championshipId ? championshipId : "";
    const swimmerName = swimmerNameId ? swimmerNameId : "";
    const isLicense = isLicenseId ? isLicenseId != "0" : false;
    const [formData, setFormData] = useState({
        participant_license: isLicense ? swimmerName : "",
        participant_name: isLicense ? "" : swimmerName,
        isLicense: isLicense
    });
    const getSwimmerValue = () => {
        return isLicense ? formData.participant_license : formData.participant_name;
    };
    const getSearchFilterFromFromData = () => {
        if (formData.participant_name == "" && formData.participant_license == "") { return ""; }
        const participant_filter = formData.isLicense ? `participant_license=${formData.participant_license}` : `participant_name=${formData.participant_name}`;
        return `${inscriptions_api_uri}?championship_name=${encodeURI(championship)}&${participant_filter}&results_limit=100`;
    };
    const [searchFilter, setSearchFilter] = useState(getSearchFilterFromFromData());
    const handleIsLicenseChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = event.target.checked;
        setFormData(change);
    };
    const handleFormChange = (event: any) => {
        let change: any = {...formData};
        change[event.target.name] = event.target.value;
        setFormData(change);
    };
    const history = useHistory();
    const handleSubmit = (event: any) => {
        event.preventDefault();
        const newPath = "/"+["swimmersearch", championship, getSwimmerValue(), isLicense?"1":"0"].join("/");
        history.push(newPath);
        setSearchFilter(getSearchFilterFromFromData());
    };
    const location = useLocation();
    useEffect(() => {
        setSearchFilter(getSearchFilterFromFromData());
    }, [location]);
    const searchButtonStyle ={
        height: "auto",
        marginTop: "auto",
        marginBottom: "10px"
    };
    return(
        <>
            <div className="form-row align-items-center">
                <div className="col-auto">
                    <div className="form-check form-row">
                        <input type="checkbox" name="isLicense" className="form-check-input" checked={formData.isLicense} onChange={handleIsLicenseChange}/>
                        <label htmlFor="licenciaActiva">Busqueda por licencia</label>
                    </div>
                    <div className="form-group form-row">
                        <input type="text" name={formData.isLicense ? "participant_license" : "participant_name"} placeholder={formData.isLicense ? "licencia" : "nombre"}
                               value={formData.isLicense ? formData.participant_license : formData.participant_name} onChange={handleFormChange} className="form-control"/>
                    </div>
                </div>
                <div className="col-auto" style={searchButtonStyle}>
                    <button className="align-self-end btn btn-lg btn-block btn-primary" onClick={handleSubmit}>Buscar</button>
                </div>
            </div>
            {searchFilter === "" ? <></> : <CompetyTableController columnsDefinition={swimmerSearchTableColumns} searchFilter={searchFilter} />}
        </>
    );
};

export default SwimmerSearch;