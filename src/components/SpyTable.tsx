import React, {useEffect, useState} from "react";
import {LeveradeParticipant} from "../leverade";
import {seasons} from "../values";
import InscriptionTimeRenderer from "./tablecellrenderers/InscriptionTimeRenderer";

const LeveradeTimeRenderer = ({time}: any) => {
    return (<>{
            time !== 0
            ? <InscriptionTimeRenderer values={{time: time}}/>
            : <></>
        }</>);
}

const SpyTableElement = ({leveradeParticipant, event, pool, season}: any) => {
    const [time, setTime] = useState(0)
    const [date, setDate] = useState("")
    useEffect(() => {
        leveradeParticipant.get_event_results(event.distance, event.style, pool, 1, season, setDate, setTime)
    }, [leveradeParticipant, pool, event, season]);
    return(<><td><LeveradeTimeRenderer time={time}/></td><td>{date}</td></>);
}

const SpyTable = ({participant: {leverade: {name, surname}, born_year}}: any) => {
    const [season, setSeason] = useState("all");
    const leveradeSwimmer = new LeveradeParticipant(name, surname, born_year)
    const freeEvents = [50, 100, 200, 400, 800, 1500].map(d => { return {distance: d, style: "LIBRE"} })
    const butterflyEvents = [50, 100, 200].map(d => { return {distance: d, style: "MARIPOSA"} })
    const backEvents = [50, 100, 200].map(d => { return {distance: d, style: "ESPALDA"} })
    const breastEvents = [50, 100, 200].map(d => { return {distance: d, style: "BRAZA"} })
    const medlyEvents = [100, 200, 400].map(d => { return {distance: d, style: "ESTILOS"} })
    const eventsList = [...freeEvents, ...butterflyEvents, ...backEvents, ...breastEvents, ...medlyEvents];
    const handleSeasonChange = (event: any) => {
        setSeason(event.target.textContent);
    };
    const seasonStyle ={
        height: "auto",
        marginTop: "auto",
        marginLeft: "10px"
    };
    return (
        <>
            <div className="form-row align-items-center">
                <div className="col-auto dropdown">
                    <button className="form-group form-row btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Temporada
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <span key="all" className="dropdown-item" onClick={handleSeasonChange}>all</span>
                        {seasons.map((ss) => <span key={ss} className="dropdown-item" onClick={handleSeasonChange}>{ss}</span>)}
                    </div>
                </div>
                <div className="col-auto">
                    <div className="form-group form-row" style={seasonStyle}>
                        {season}
                    </div>
                </div>
            </div>
            <table id="tabla_tiempos" className="display table">
                <thead>
                <tr>
                    <td rowSpan={2} />
                    <th colSpan={2}>25m</th>
                    <th colSpan={2}>50m</th>
                </tr>
                <tr>
                    <th>Tiempo</th>
                    <th>Fecha</th>
                    <th>Tiempo</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>
                    {eventsList.map(event => {
                        return (
                            <tr key={event.distance.toString()+event.style+leveradeSwimmer.get_filter()+season}>
                                <th scope="row">{event.distance}m {event.style.toLowerCase()}</th>
                                <SpyTableElement leveradeParticipant={leveradeSwimmer} event={event} pool={25} season={season} />
                                <SpyTableElement leveradeParticipant={leveradeSwimmer} event={event} pool={50} season={season} />
                            </tr>);
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SpyTable;