import EventRenderer from "./components/tablecellrenderers/EventRenderer";
import InscriptionTimeRenderer from "./components/tablecellrenderers/InscriptionTimeRenderer";
import SpyRenderer from "./components/tablecellrenderers/SpyRenderer";
import React from "react";

export const swimmerSearchTableColumns = [
    {
        accessor: 'participant',
        Header: 'Spy',
        Cell: ({ row, cell: { value } }: any) => <SpyRenderer row={row} values={value} />
    }, {
        accessor: 'participant.name',
        Header: 'Nombre'
    }, {
        accessor: 'event',
        Header: 'Prueba',
        Cell: ({ cell: { value } }: any) => <EventRenderer values={value} />,
    }, {
        accessor: 'club',
        Header: 'Club'
    }, {
        accessor: 'initial_position',
        Header: 'Posicion'
    }, {
        accessor: 'inscription_time',
        Header: 'Tiempo',
        Cell: ({ cell: { value } }: any) => <InscriptionTimeRenderer values={value} />,
        id: "time"
    }, {
        accessor: 'inscription_time',
        Header: 'Piscina',
        Cell: ({ cell: { value } }: any) => <>{value.pool_length}-{value.chronometer_type}</>,
        id: "pool"
    }, {
        accessor: 'heat',
        Header: 'Serie'
    }, {
        accessor: 'lane',
        Header: 'Calle'
    }, {
        accessor: 'participant.born_year',
        Header: 'Nacimiento'
    },
]

export const eventSearchTableColumn = [
    {
        accessor: 'participant',
        Header: 'Spy',
        Cell: ({ row, cell: { value } }: any) => <SpyRenderer row={row} values={value} />
    }, {
        Header: "#",
        id: "row",
        filterable: false,
        Cell: (values: any) => <div>{values.row.index + 1}</div>
    }, {
        accessor: 'participant.name',
        Header: 'Nombre'
    }, {
        accessor: 'club',
        Header: 'Club'
    }, {
        accessor: 'inscription_time',
        Header: 'Tiempo',
        Cell: ({ cell: { value } }: any) => <InscriptionTimeRenderer values={value} />,
        id: "time"
    }, {
        accessor: 'inscription_time',
        Header: 'Piscina',
        Cell: ({ cell: { value } }: any) => <>{value.pool_length}-{value.chronometer_type}</>,
        id: "pool"
    }, {
        accessor: 'heat',
        Header: 'Serie'
    }, {
        accessor: 'lane',
        Header: 'Calle'
    }, {
        accessor: 'participant.born_year',
        Header: 'Nacimiento'
    },
]

export const eventRelaySearchColumns = [
    {
        Header: "#",
        id: "row",
        filterable: false,
        Cell: (values: any) => <div>{values.row.index + 1}</div>
    }, {
        accessor: 'club',
        Header: 'Club'
    }, {
        accessor: 'inscription_time',
        Header: 'Tiempo',
        Cell: ({ cell: { value } }: any) => <InscriptionTimeRenderer values={value} />,
        id: "time"
    }, {
        accessor: 'inscription_time',
        Header: 'Piscina',
        Cell: ({ cell: { value } }: any) => <>{value.pool_length}-{value.chronometer_type}</>,
        id: "pool"
    }, {
        accessor: 'heat',
        Header: 'Serie'
    }, {
        accessor: 'lane',
        Header: 'Calle'
    }, {
        accessor: 'event.category',
        Header: 'Categoria'
    }
]