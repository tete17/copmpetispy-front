export const range = (start: number, stop: number, step: number) => Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));
export const distances = [50, 100, 200, 400, 800, 1500];
export const relayDistances = [50, 100];
export const styles = ["Mariposa", "Espalda", "Braza", "Libre", "Estilos"];
export const genders = ["Masculino", "Femenino", "Mixto"];
export const categories = range(20, 100, 5);
export const relayCategories = [80, 100, 120, 160, 240, 280];
export const seasons : string[] = [
    '20/21',
    '19/20',
    '18/19',
    '17/18',
    '16/17',
    '15/16',
    '14/15',
    '13/14',
    '12/13',
    '11/12',
    // '10/11',  // No data found in leverade database from this point
];
export const inscriptions_api_uri : string = "/api/v1/inscriptions";
export const championships_api_uri : string = "/api/v1/championships";