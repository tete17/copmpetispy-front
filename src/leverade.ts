import {cachedFetch} from "./catched_fetch";
import {seasons} from "./values";

declare global {
    interface String {
        format(...args: any[]) : string;
        removeLastChar() : string;
    }
}

String.prototype.format = function (...args: any[]) {
    let _args = [].slice.call(arguments);
    return this.replace(/(\{\d+\})/g, function (a){
        return _args[+(a.substr(1,a.length-2))||0];
    });
};

String.prototype.removeLastChar = function (this: string) {
    return this.substr(0, this.length - 1);
};

export const Leverade_SEASON : any = {
    '20/21': 4130,
    '19/20': 3331,
    '18/19': 2621,
    '17/18': 2620,
    '16/17': 2619,
    '15/16': 2618,
    '14/15': 2617,
    '13/14': 2616,
    '12/13': 2615,
    '11/12': 2614,
    // '10/11': 2613,  // No data found in leverade database from this point
};

class Leverade {
    public static STYLE : any = {'braza': 6, 'espalda': 4, 'estilos': 8, 'libre': 5, 'mariposa': 7};

    static get_request(filters: any, callback: any, result_size=50, result_include: any=null, page_number=1) {
        let filter_string = filters.join(',');
        if (filter_string.endsWith(',')) {
            filter_string = filter_string.removeLastChar();
        }
        let payload: any = {
            'sort': 'value',
            'filter': '!resultable[license].id:null,style.discipline.id:38,official:true,value>0,{0}'.format(filter_string),
            'page[number]': page_number,
            'page[size]': result_size,
        };
        if (result_include) {
            payload['include'] = result_include;
        }
        let payload_array = [];
        for (let key in payload) {
            payload_array.push('{0}={1}'.format(key, payload[key]));
        }
        const query = "https://api.leverade.com/managers/210453/individual_phaseresults?{0}".format(payload_array.join('&'))
        const cacheExpirySeconds = 24*60*60; // One day in seconds
        cachedFetch(cacheExpirySeconds, query).then((res) => {
                if (res.ok) {
                    return res.json()
                } else {
                    throw new Error(res.statusText);
                }
            })
            .then(res => callback(res))
            .catch(err => console.log('Error: ', err))
    }

    static get_request_data(filters: any, setDate: any, setTime: any, result_size=50, result_include: any=null, page_number=1) {
        const updateTimeAndData = (queryResult: any) => {
            if (queryResult.length === 0) { return; }
            const date_parts = queryResult[0]["attributes"]["date"].split(' ')[0].split('-');
            const date_print = '{0}/{1}/{2}'.format(date_parts[2], date_parts[1], date_parts[0]);
            setDate(date_print);
            setTime(queryResult[0]["attributes"]["value"]/100.0);
        }
        Leverade.get_request(filters,
            (data: any) => {updateTimeAndData(data['data'])},
            result_size, result_include, page_number)
    };

    static find_leverade_participant_profile(first_name: string, last_name: string, birth_year: number, callback: any) {
        let find_profile_callback = function (response_data: any) {
            let found_profiles: any = [];
            if ('included' in response_data) {
                response_data['included'].forEach(function(element: any) {
                    if (element['type'] === "profile") {
                        element['birth_year'] = birth_year;
                        found_profiles.push(element);
                    }
                });
            }
            if (found_profiles.length === 1) {
                callback(found_profiles[0]);
            }
        };
        Leverade.get_request([Leverade.get_aproximate_profile_filter(first_name, last_name, birth_year)],
            find_profile_callback, 25, 'resultable.profile');
    };

    static get_aproximate_profile_filter(first_name: string, last_name: string, birth_year: number) {
        return "profile.first_name^{0},profile.last_name^{1},profile.birthdate>{2},profile.birthdate<{3}".format(
            first_name.replace(/ /g, '+'), last_name.replace(/ /g, '+'), Leverade.get_last_day_prev_year(birth_year),
            Leverade.get_first_day_next_year(birth_year));
    };

    static get_event_filter(distance: number, style: string) {
        return 'goal:{0},style.id:{1}'.format(distance, Leverade.STYLE[style.toLowerCase()])
    };

    static get_season_filter(season: string) {
        const indx : number = seasons.indexOf(season);
        return indx != -1 ? 'season.id:{0}'.format(Leverade_SEASON[seasons[indx]]) : "";
    };

    static get_pool_filter(pool: number) {
        return 'discipline_fields.pool_size:{0}'.format(pool)
    };

    static get_last_day_prev_year(year: number) {
        return '{0}-12-31'.format(year - 1);
    };

    static get_first_day_next_year(year: number) {
        return '{0}-01-01'.format(year + 1);
    };
}

export class LeveradeParticipant {
    private readonly profile_filter: any;

    constructor(name: string, surname: string, birth_year: number) {
        this.profile_filter = Leverade.get_aproximate_profile_filter(name, surname, birth_year)
    }

    get_filter() : string {
        return this.profile_filter;
    }

    get_event_results(distance: number, style: string, pool: any=null, result_size: number=10, season: string="all", setDate: any, setTime: any) {
        let filters = [this.profile_filter, Leverade.get_event_filter(distance, style)];
        if (pool != null) {
            filters.push(Leverade.get_pool_filter(pool));
        }
        const seasonfilter = Leverade.get_season_filter(season);
        if (seasonfilter != "") {
            filters.push(seasonfilter);
        }
        Leverade.get_request_data(filters, setDate, setTime, result_size);
    }
}

